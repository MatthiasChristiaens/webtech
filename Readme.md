# Webtech 2 portfolio #

Voor het vak webtechnologie hebben we dit semester enkele verschillende technieken om mogelijk
mee aan de slag te gaan in het beroepsleven. Proeven van alles en kiezen in wat jij je wil specialiseren was de boodschap.
Daarom kregen we elke week een ander deel leerstof en elke week een opdracht over de leerstof tegen de volgende week.

Op deze repository kan je al mijn opdrachten vinden en hoe ik ze heb aangepakt. Ik som ze even voor je op!

*  Github 
*	Css animations & transitions [Lab 2](https://bitbucket.org/MatthiasChristiaens/webtech/src/6928c455bc6aaa77fd1f35c28b5a1fbd6d43feca/Lab2Webtech/?at=master)
*  Advanced Javascript [Lab 3](https://bitbucket.org/MatthiasChristiaens/webtech/src/6928c455bc6aaa77fd1f35c28b5a1fbd6d43feca/Lab3Webtech/Todo/?at=master)
*  App prototype [Lab 4](https://bitbucket.org/MatthiasChristiaens/webtech/src/6928c455bc6aaa77fd1f35c28b5a1fbd6d43feca/Lab4Webtech/?at=master)
*  Node.js [Lab 5](https://bitbucket.org/MatthiasChristiaens/webtech/src/6928c455bc6aaa77fd1f35c28b5a1fbd6d43feca/Lab5Webtech/?at=master, )
* Node.js extension [Lab 5](https://bitbucket.org/MatthiasChristiaens/webtech/src/6928c455bc6aaa77fd1f35c28b5a1fbd6d43feca/Lab7Webtech/?at=master)
*  Angular [Lab 6](https://bitbucket.org/MatthiasChristiaens/webtech/src/6928c455bc6aaa77fd1f35c28b5a1fbd6d43feca/Lab6Webtech/Digital-Bar%20Base/?at=master)
*  Sass [Lab 7]
*  Gulp [Lab 8]

Dit was een opsomming van de labs + de hyperlink naar de labs in mijn webtech repository.

Github
------
####Wat is Github?

Git is een programma waar je projecten kan in opslaan. Het is een handige tool om aan projecten te werken met groepen personen. Je kan elk je eigen "branch" aanmaken. Een soort aftakking van het grote algemene project waar je zelf aan jou deel kan werken. Ook staat deze code altijd online dus als je per ongeluk iets verkeerd doet met je computer kan je nog steeds aan de code!

Github en Bitbucket zijn de bekendste. Wij kozen in dit geval voor Bitbucket omdat je hier private repositories kan aanmaken. Repositories zijn werkmappen waar gegevens worden in opgeslagen. Deze repositories kunnen ook makkelijk gedeeld worden met andere mensen dus zeer handig indien je aan een groot project wilt werken met meerdere personen.

####Begrippenlijst

Repository: Is een werkmap.

Fork: Is een kopie maken van een andere repository.

Branch: Is op de repository een apart deel aanmaken dat compleet van het totale project staat om zo aan je eigen deel van het project kunt werken zonder dat anderen hier last van zouden ondervinden. Later kan je de branch samenvoegen met de master.

Pull request: Als je een fork hebt gemaakt van een project om hier aanpassingen aan te doen, wil je natuurlijk ook dat mensen je werk zien als je hier content van bent. Daarom kan je een pull request doen om jou uitgebreide kopie terug door te sturen. De beheerder kan dan je aanpassingen goedkeuren of afkeuren naar gelang het goed of slecht is.

####Commands

In de lessen hebben we met gitshell gewerkt, een command prompt voor Git. Ik daarintegen koos voor de grafische interface van Git die voor mij veel makkelijker hanteerbaar was omwille van de visuele stappen die ik deed.
####Lab 1 uitleg

We moesten onze eigen repository aanmaken in groepjes, daar moesten we elk één html pagina voor schrijven en deze dan terug committen. Daarna moesten wij een pull request doen en dat was het einde van onze opdracht.

CSS animations and transitions
------------------------------

De tweede week hebben wij zitten spelen met de nieuwe CSS3 animations. Zo konden wij leuke animaties maken zonder al te veel code. 

####Enkele voorbeelden

###Transitie

	#circle
	{
		display: block;
		width: 200px;
		height: 200px;
		border-radius: 50%;
		background-color: red;
		transition: background-color 2s ease-in 1s;
	}
	
	#circle:hover
	{
		background-color: red;
	}

De transitie mag hier niet meteen worden uitgevoerd, dat geeft een lelijk beeld. Daarom zetten we een ease-in op onze circle div om zo een overgangsgevoel te krijgen. De kleur veranderd van de cirkle na 2 seconden naar rood als je erover hovert.

###Transformatie

	#block
	{
		display: block;
		width: 100px;
		height: 100px;
		background-color: green;
    	transition: -webkit-transform 1s;
	}
	
	#block:hover
	{
		-webkit-transform: translate(10px, 20px) 
		scale(0.5) rotate(15deg) skewX(10deg);
	}


###Animaties

	.animate
	{
		display: block;
  		width: 100px;
  		height: 100px;
  		background-color: red;
  		-webkit-animation: animatie 5s ease-in infinite alternate;
	}

	@-webkit-keyframes animatie
	{
		0%
		{}
		25%
		{-webkit-transform: translateX(100px);}
		50%
		{-webkit-transform: translateX(200px);}
		75%
		{-webkit-transform: translateX(300px);}
		100%
		{}
	}


Advanced Javascript
-------------------
Advanced javascript bouwt verder op de standaard javascript die wij vorig jaar hebben gezien. In plaats van Jquery te laden konden wij zo onze eigen code schrijven om Jquery te kunnen gebruiken. Extra kennis van Javascript was daarom vereist. Als opdracht hebben wij een To-Do listje uitgewerkt dat volledig functioneel is.

###Voorbeeld
	var $ = function(selector)
	{
	// check if selector is an object already e.g. by passing 'this' on clicks
	if(typeof(selector) == "object")
	{
		return new WrapperElement(selector);
	}

	// get the first character to know if we want an element, id or class
	var firstCharacter = selector.charAt(0);
	var selectedItems;

	switch(firstCharacter)
	{
		case '#':
		var elementid =	selector.substr(1);
		selectedItems = document.getElementById(elementid);
		break;

		case '.':
		var elementclass = selector.substr(1);									

		selectedItems = document.getElementsByClassName(elementclass);			
		break;
		
		default:
		selectedItems = document.getElementsByTagName(selector);				
																				
	}

	var newElement = new WrapperElement(selectedItems);
	return newElement;

	}



App prototype
-------------
Een gewone website maken was iets dat we vorig jaar leerden. Nu gaan we een stapje verder door een applicatie prototype te bouwen. Door met een API te werken konden wij onze eigen weerapplicatie maken die het weer van de dag liet zien op de locatie van uw laptop.


###Voorbeeld
	{
						type: 'GET', 
						dataType: 'JSONP',
						url: "https://api.forecast.io/forecast/9e4ed19b1980e149bc14a1aed0ac982f/"+position.coords.latitude + "," + position.coords.longitude,
						success: function(resp)
						{

							w.innerHTML = Math.round((resp.currently.temperature - 32)/1.8) + " <strong>&degC</strong>"; 
							om.innerHTML = resp.currently.summary;
							min.innerHTML = "Min: " + Math.round((resp.daily.data[0].temperatureMin-32)/1.8) + " &degC";
							max.innerHTML = "Max: " + Math.round((resp.daily.data[0].temperatureMax-32)/1.8) + " &degC";

Deze code laadt de API van forecast.io en vraagt om uit de API wat data te halen zoals de minimum en maximumtemperatuur van het weer van vandaag. En zet ze daarna om in html.

Node.js
-------
Node.js is een back-end variant op de front-end javascript die wij reeds geleerd hebben. Met Node.js hebben wij 2 opdrachten gemaakt namelijk, een applicatie die drankjes kan bijhouden, je bestelling opslaan, nieuwe drankjes toevoegen en deze doorsturen. De 2de opdracht was een Q&A applicatie waarbij de messages live werden geupdate. 

###Voorbeeld

	socket.on('startMessages', function(products) {

					$('#products').val();
					var length = products.length;
					for(var i = 0; i < products.length; i++) {

						//console.log('update', products[i]._id);
						$('#products').append('<li id="'+products[i]._id+'">'+products[i].name+'<span id="aantal">'+' '+products[i].amount+'</span>' +' '+ '<input type="button" id="plus" value="+" class="btn btn-success"><input type="button" id="min" value="-" class="btn btn-danger"></li>');
					}
				});
Deze code zorgt er **live**, via websockets, voor dat alle records van messages uit de database worden gelezen en deze mooi in een lijst worden gezet in html

Angular
-------
Angular is een framework voor javascript om makkelijk Single Page Applications te schrijven. De applicatie wordt binnen één html-pagina geschreven. Een reload is dus niet nodig als je naar andere views wil gaan omwille van het feit dat je slechts één html-pagina hebt. Als opdracht moesten wij weer een applicatie bouwen die drankjes kon bijhouden, een bestelling kon opslaan en nieuwe drankjes kon aanmaken + bijhouden.

###Voorbeeld

	<div class="orderAdd">
                    <input type="text" placeholder="Zoek een product" ng-model="productSearch"/>

                    <ul class="productList" ng-init = "init()">
                        <li ng-repeat="product in products | filter:productSearch">
                            
                            <span class="name">{{product.name}}</span>

                            <small class="quantity">{{product.quantity || 0}}</small>

                            <div class="controlls">
      						  	<button ng-click="product.quantity = product.quantity + 1">+</button>
       						 	<button ng-click="product.quantity = product.quantity - 1">-</button>
    						</div>
Deze code zorgt ervoor dat je een search functie hebt om een drankje te zoeken. Ook heb je twee buttons die je hoeveelheid van je drankje kan optellen of aftrekken.

Sass
----
Sass staat voor Syntactically Awesome Stylesheets. Sass is een CSS-preprocessor. Wij schreven vroeger altijd alle CSS code in één stylesheet.

###Voorbeeld

	/css
		/style.css

Terwijl met Scss files zouden je stylesheets er zo uitzien:

	/scss
		/style.scss
		/modules
			/_nav.scss
			/_sidebar.scss
			/_footer.scss

in de style.scss komen alle modules samen via een

	@import

Om deze allemaal te groeperen en uiteindelek om te zetten naar je oude vertrouwe

	style.css
	

Gulp
----
Gulp is een tool om javascript, css-files en images te minifyen. Dit betekent dat deze minder geheugen vereisen als de gebruiker je site bezoekt. Het handige hieraan is dat de gebruiker geen, bij wijze van spreken, uren moet wachten tot de site geladen is. 

Een voorbeeld van Gulp is dus bijvoorbeeld het minifyen van een sass bestand.

###Voorbeeld

	var gulp = require('gulp);
	var sass = require('gulp-ruby-sass');
	var minifycss = require('gulp-minify-css');
	
	gulp.task('styles', function(){
		return gulp.src('source van het scss-bestand')
			.pipe(sass())
			.pipe(gulp.dest('destination van de css-file'));
	});
	
	gulp.task('default', ['styles]);
