(function ()
		{

		/*global $, jQuery*/
		"use strict";

		var App, myApp;
		App = function()
		{
			this.showTemperature = function()
			{

				if (localStorage.getItem('temperature') !== null )
				{
					var temperature = JSON.parse(localStorage.getItem('temperature'));
				}


				var url = "https://api.forecast.io/forecast/9e4ed19b1980e149bc14a1aed0ac982f/51.016666699999990000,4.466666700000019000";

				$.ajax(
					{
						type: 'GET', 
						dataType: 'JSONP',
						url:url, 
						success: function(resp)
						{
							var temperature = resp.temperature;

							localStorage.setItem("temperature", JSON.stringify(temperature));

							$.each(temperature, function(key, temp)
							{
								console.log(temp.name);
							});
						},
						error: function()
						{

						}
					});

			};
		};

		myApp = new App();
		myApp.showTemperature();

	}());